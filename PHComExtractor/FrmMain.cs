﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.ServiceModel.Syndication;
using System.Xml;
using PHComExtractor.Properties;
using System.IO;
using OfficeOpenXml;

namespace PHComExtractor
{
    public partial class FrmMain : Form
    {
        DataTable dtResult = new DataTable();

        public FrmMain()
        {
            InitializeComponent();
        }

        private void btnSearch_Click(object sender, EventArgs e)
        {
            string apiUrl = Settings.Default.apiUrl;
            string paramWhat = Uri.EscapeDataString(txtWhat.Text);
            string paramWhere = Uri.EscapeDataString(txtWhere.Text);
            string apiRequest = String.Empty;

            if (!String.IsNullOrEmpty(paramWhat) && !String.IsNullOrEmpty(paramWhere))
            {
                apiRequest = String.Format("{0}?what={1}&where={2}", apiUrl, paramWhat, paramWhere);
                XmlReader reader = XmlReader.Create(apiRequest);
                SyndicationFeed feed = SyndicationFeed.Load(reader);
                reader.Close();

                DataTable dt = this.FeedDataTable();

                foreach (SyndicationItem item in feed.Items)
                {
                    DataRow dr = dt.NewRow();
                    dr["Title"] = item.Title.Text;

                    foreach (SyndicationLink links in item.Links)
                    {
                        if (!String.IsNullOrEmpty(dr["Url"].ToString()))
                        {
                            dr["Url"] = dr["Url"] + "," + links.Uri;
                        }
                        else
                        {
                            dr["Url"] = links.Uri;
                        }
                    }

                    dr["Description"] = item.Summary.Text;
                    dt.Rows.Add(dr);
                }

                if (dt.Rows.Count > 0)
                {
                    MessageBox.Show("Found " + dt.Rows.Count.ToString() + " result(s)", "Notice",  MessageBoxButtons.OK, MessageBoxIcon.Information);
                }

                dtResult = dt;
                dgResult.DataSource = dtResult;

            }
            else
            {
                MessageBox.Show("Please provide search parameter for both what and where", "Notice", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
            }
        }

        protected DataTable FeedDataTable()
        {
            DataTable dt = new DataTable();
            dt.Columns.Add(new DataColumn("Title", typeof(string)));
            dt.Columns.Add(new DataColumn("Url", typeof(string)));
            dt.Columns.Add(new DataColumn("Description", typeof(string)));
            return dt;
        }

        protected void ExtractDataFromString(string QueryString)
        {
            int indexBt = QueryString.IndexOf("Business Type:");
            int indexAddress = QueryString.IndexOf("Address:");
        }

        private void btnDownload_Click(object sender, EventArgs e)
        {
            if (dtResult.Rows.Count > 0)
            {

                SaveFileDialog sfd = new SaveFileDialog();
                sfd.FileName = String.Format("PHComExtract-{0:yyyy.MM.dd-hhmmtt}", DateTime.Now);
                sfd.Filter = "Excel File (*.xlsx)|";

                DialogResult result = sfd.ShowDialog();

                if (result == System.Windows.Forms.DialogResult.OK && !String.IsNullOrEmpty(sfd.FileName))
                {
                    string file = sfd.FileName + ".xlsx";
                    using (var excel = new ExcelPackage(new FileInfo(file)))
                    {
                        string workBookName = "Sheet1";
                        DataTable dt = new DataTable();
                        var ws = excel.Workbook.Worksheets.Add(workBookName);
                        ws.Workbook.Worksheets[workBookName].Cells["A1"].LoadFromDataTable(dtResult, true);
                        excel.Save();
                    }
                    MessageBox.Show("Download complete", "Notice",  MessageBoxButtons.OK, MessageBoxIcon.Information);
                }
            }
            else
            {
                MessageBox.Show("Nothing to download", "Notice", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
            }
        }
    }
}
